class Subject < ActiveRecord::Base
   validates :kamoku, presence: true, uniqueness: true
    validates :professor, presence: true, uniqueness: true
 has_many :schedules
 has_many :items
 belongs_to :student
 belongs_to :kanri
end
