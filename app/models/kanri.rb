class Kanri < ActiveRecord::Base
   has_many :subject
  validates :ka_id, presence: true, uniqueness: true
  validates :password, presence: true, confirmation: true
    attr_accessor :password
    def password=(val)
    if val.present?
    self.ka_pass = BCrypt::Password.create(val)
    end
     @password = val
    end
   
   def self.authenticate(ka_id, ka_pass)
    kanri = Kanri.find_by_ka_id(ka_id)
    if kanri && BCrypt::Password.new(kanri.ka_pass) == ka_pass
      kanri
    else
      nil
    end
   end
end
