class SubjectsController < ApplicationController
  def index
    @subjects = Subject.all
  end

def new
   @subject = Subject.new
end
  
  def create
    @subject = Subject.new(
    kamoku: params[:subject][:kamoku], 
    professor: params[:subject][:professor],
    unit: params[:subject][:unit],
    youbi: params[:subject][:youbi],
    zigen: params[:subject][:zigen])
    if @subject.save
      flash[:touroku_kougi] = "講義を追加しました。"
      
      redirect_to tops_main_path
      
    else
      render 'new'
    end
  end

def edit
  @subject =Subject.find(params[:id])
end

def update
   @subject =Subject.find(params[:id])
    kamoku = params[:subject][:kamoku]
    professor = params[:subject][:professor]
    unit = params[:subject][:unit]
    youbi = params[:subject][:youbi]
    zigen = params[:subject][:zigen]
  @subject.update(kamoku: kamoku) 
  @subject.update(professor: professor)
  @subject.update(unit: unit)
  @subject.update(youbi: youbi)
  @subject.update(zigen: zigen)
   redirect_to tops_main_path
   flash[:syuusei_kougi] = "講義を修正しました。"
end 

 def destroy
  Subject.find(params[:id]).destroy
   redirect_to tops_main_path
   flash[:sakuzyo_kougi] = "講義を削除しました。"
 end 
end
