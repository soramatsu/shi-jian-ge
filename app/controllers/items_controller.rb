class ItemsController < ApplicationController
  def index
  @items = Item.all
  end
  
  def new
   @subjects = Subject.all
   @item = Item.new
  end

def create
      @item = Item.new(
    student_id: current_user.id,
    subject_id: params[:subject_id],
    kamoku: params[:kamoku], 
    professor: params[:professor],
    unit: params[:unit],
    youbi: params[:youbi],
    zigen: params[:zigen]
    )
    @item.save
     redirect_to new_item_path
end

def show
  @items = Item.all
end

def destroy
    Item.find(params[:id]).destroy
    redirect_to item_path
end
end
