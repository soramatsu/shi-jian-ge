class KanrisController < ApplicationController
  def index 
    @kanris = Kanri.all
  end
    
  def new
   @kanri = Kanri.new
  end
  
  def create
    @kanri = Kanri.new(
     ka_id: params[:kanri][:ka_id],
     password: params[:kanri][:password],
     password_confirmation: params[:kanri][:password_confirmation])
     flash[:touroku] = "管理者を登録しました。"
    if @kanri.save
      redirect_to tops_ka_login_path
    else
      render 'new'
    end
  end

 def destroy 
  Kanri.find(params[:id]).destroy
  session.delete(:ka_login_uid)
    redirect_to kanris_path
    flash[:sakuzyo] = "管理者を削除しました。"
 end 
end
