Rails.application.routes.draw do
  resources :kanris, only: [:create, :destroy,:edit,:view,:index,:new]
  get  'tops/main'
  root 'tops#main'
  get 'tops/st_login'
  post 'tops/st_login'
  get 'tops/friend'
  post 'tops/friend'
  get  'tops/st_logout'
  get 'tops/ka_login'
  post 'tops/ka_login'
  get  'tops/ka_logout'
  post 'subjects/:id/edit' => 'subjects#edit'
  get 'students/search'
  post 'students/search'
    get 'students/itiran'
  post 'students/itiran'
  resources :subjects, only: [:index, :new, :create, :destroy,:update,:edit]
  resources :students, only: [:index, :new, :create, :destroy,:update,:edit]
  resources :items, only: [:index,:show,:new,:create,:destroy]
  resources :schedules, only: [:index,:show,:create]
  
end
