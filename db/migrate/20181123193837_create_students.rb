class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :st_id
      t.string :st_pass
      t.string :st_name

      t.timestamps null: false
    end
  end
end
