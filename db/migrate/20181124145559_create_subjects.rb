class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :kamoku
      t.string :professor
      t.integer :unit
      t.string :youbi
      t.integer :zigen
      t.string :mark

      t.timestamps null: false
    end
  end
end
