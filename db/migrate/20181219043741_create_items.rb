class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :student_id
      t.integer :subject_id
      t.string :kamoku
      t.string :professor
      t.integer :unit
      t.string :youbi
      t.integer :zigen
      t.timestamps null: false
    end
  end
end
